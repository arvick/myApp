-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 08, 2017 at 01:28 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `emp_data`
--

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE IF NOT EXISTS `projects` (
  `prj_id` int(11) NOT NULL AUTO_INCREMENT,
  `project_name` varchar(50) NOT NULL,
  `country` varchar(50) NOT NULL,
  PRIMARY KEY (`prj_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`prj_id`, `project_name`, `country`) VALUES
(3, 'Ntt Italy', 'Italy'),
(4, 'makeson', 'UK'),
(5, 'wells Fargo', 'United State');

-- --------------------------------------------------------

--
-- Table structure for table `user_details`
--

CREATE TABLE IF NOT EXISTS `user_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `projectId` int(11) NOT NULL,
  `designation` varchar(50) NOT NULL,
  `empCode` varchar(50) NOT NULL,
  `ranking` int(11) NOT NULL,
  `image` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=33 ;

--
-- Dumping data for table `user_details`
--

INSERT INTO `user_details` (`id`, `name`, `projectId`, `designation`, `empCode`, `ranking`, `image`, `password`) VALUES
(1, 'abdul rahim', 3, 'UI Developers', '1081474', 5, 'uploads/DSC_0034.jpg', 'testadmi'),
(3, 'indraja nutalapati', 4, 'Ass. Software developer', '108146', 4, 'uploads/test1.jpg', 'test'),
(4, 'Arockiam Paul', 0, 'Technical Lead', '109876', 5, 'uploads/test1.jpg', 'test'),
(28, 'subeen', 0, 'Team lead', '3330h', 3, 'uploads/DSC_0034.jpg', 'test'),
(31, 'test', 0, 'test', '444', 4, 'uploads/DSC_0034.jpg', 'test'),
(32, 'test', 4, 'test', '444', 4, 'uploads/test1.jpg', 'test');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
