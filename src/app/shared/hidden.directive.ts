import { Directive, ElementRef, Renderer, OnInit, Input } from '@angular/core';

@Directive({
  selector: '[appHidden]'
})
export class HiddenDirective implements OnInit {

  constructor(private el:ElementRef, private rendrer:Renderer) { 

     // rendrer.setElementStyle(el.nativeElement,'display','none');

  }

  @Input() appHidden:boolean;

  ngOnInit(){

    console.log(this.appHidden);
    if(this.appHidden)
      this.rendrer.setElementStyle(this.el.nativeElement,'display','none');

  }

}
