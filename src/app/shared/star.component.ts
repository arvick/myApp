import { Component, Input, OnChanges, Output, EventEmitter } from '@angular/core';

@Component({
    selector:'star-tmp',
    templateUrl:'./star.component.html',
    styleUrls: ['./shared.component.css']
})
export class StarComponent implements OnChanges{

  @Input() rating:number;
  starWidth:number;

  @Output() starClicked: EventEmitter <string> = new EventEmitter<string>();

  ngOnChanges():void{

        this.starWidth=this.rating * 86/5;

  }

  onClick():void{
    this.starClicked.emit(`${this.rating} is clicked`);
  }
}