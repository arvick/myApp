import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';


import { AppComponent } from './app.component';
import { SharedComponent } from './shared/shared.component';
import { StarComponent } from "./shared/star.component";
import { ConvertToSpace } from "./shared/convertToSpaces.pipe";
import {UserComponent } from './user/user.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { UserdetailComponent } from './userdetail/userdetail.component';
import { RouterModule } from "@angular/router";
import { UserGaurdService } from './userdetail/user-gaurd.service';
import { UserRegisterComponent } from './user/user-register.component';
import { UserEditComponent } from './user/user-edit.component';
import { HiddenDirective } from './shared/hidden.directive';
import { UserDeleteComponent } from './user/user-delete.component';
import { LoginComponent } from './login/login.component';
import { AddprojectComponent } from './admin/addproject.component';
import { AdminServiceService } from "./admin/admin-service.service";
import { ProjectListComponent } from './admin/projects/project-list.component';
import { TeamsComponent } from './admin/team/teams.component';
import { AddTeamComponent } from './admin/team/add-team.component';


@NgModule({
  declarations: [
    AppComponent,
    SharedComponent,
    StarComponent,
    ConvertToSpace,
    UserComponent,
    WelcomeComponent,
    UserdetailComponent,
    UserRegisterComponent,
    UserEditComponent,
    HiddenDirective,
    UserDeleteComponent,
    LoginComponent,
    AddprojectComponent,
    ProjectListComponent,
    TeamsComponent,
    AddTeamComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forRoot([{path:'userlist',component:UserComponent},
    {path:'user/:id', canActivate:[UserGaurdService], component:UserdetailComponent},
    {path:'edituser/:id', canActivate:[UserGaurdService], component:UserEditComponent},
    {path:'remove/:id', canActivate:[UserGaurdService], component:UserDeleteComponent},
    {path:'register',component:UserRegisterComponent},
    {path:'project-list',component:ProjectListComponent},
    {path:'add-project',component:AddprojectComponent},
    {path:'add-team',component:AddTeamComponent},
    {path:'welcome',component:WelcomeComponent},
    {path:'', component:LoginComponent},
    {path:'**', redirectTo:'LoginComponent'},
  
      ])
  ],
  providers: [UserGaurdService,AdminServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
