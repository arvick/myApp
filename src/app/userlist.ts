export interface IUserlist{

    id:number;  
    name:string;
    projectId:number;
    designation:string;
    image:string;
    ranking:number;
    empCode:string;
    password:string;
}