import { Component, OnInit } from '@angular/core';
import { NgForm } from "@angular/forms/src/forms";
import { FormGroup, FormBuilder,  Validators } from "@angular/forms";
import { UserListService } from "../user.service";
import { Router } from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers:[UserListService]
})
export class LoginComponent implements OnInit {
  errorMessage: any;
  msg: any;

  lgn:FormGroup;
  constructor(fb:FormBuilder, private _userService:UserListService, private _router:Router) {

    this.lgn=fb.group({

      empCode:[null,Validators.required],
      password:[null,Validators.compose([Validators.required,Validators.minLength(8),Validators.maxLength(8)])]
    })
   }

  
  userLogin(form:any):void{

   let _query = `${JSON.stringify(form)}`;

  
    this._userService.login(_query).subscribe(msg=>{this.msg=msg
                                                        },
                                                                error=>this.errorMessage=<any>error
                                                                   );

    

  this._router.navigate(['/userlist']);   
  }
  ngOnInit() {
  }

}
