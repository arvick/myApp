import { IUserlist } from "./userlist";
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import { HttpErrorResponse, HttpHeaders } from "@angular/common/http";

@Injectable()
export class UserListService {

    constructor(private _http:HttpClient){
        
    }

     header = new Headers({ 'Content-Type': 'application/json' });
     _temp:IUserlist;
        _dataUrl:string='../assets/userlist.json';
         _dataUrlPost:string='http://localhost:8082/myApp-server/user-register.php';
         _dataUrlList:string='http://localhost:3000/api/employeeList';
         
        getUserList():Observable<IUserlist[]> {
            
            return this._http.get<IUserlist[]>(this._dataUrlList).do(data=>console.log("All"+JSON.stringify(data))).catch(this.handleError);
        }

        getUser(id:number):Observable<IUserlist[]> {
            
            return this._http.get<IUserlist[]>(`http://localhost:3000/api/employee/${id}`).do(data=>console.log("All"+JSON.stringify(data))).catch(this.handleError);
        }
        editUser(query:FormData):any{
            return this._http.post<FormData>('http://localhost:8082/myApp-server/edit-user.php',query).do(data=>console.log(JSON.stringify(data))).catch(this.handleError);
        }
        addUser(query:any):any{            
            console.log(JSON.stringify(query));
            return this._http.post<FormData>(this._dataUrlPost,query).do(data=>console.log(JSON.stringify(data))).catch(this.handleError);
        }

        login(query:string):any{
            return this._http.post<string>("http://localhost:8082/myApp-server/userLogin.php",query).do(data=>console.log(JSON.stringify(data))).catch(this.handleError);
        }
       
        removeUser(id:number):any {
            
            return this._http.post<string>("http://localhost:8082/myApp-server/delete-user.php?id="+id+"",id).do(data=>console.log("All"+JSON.stringify(data))).catch(this.handleError);
        }

        getAllProjects():Observable<any> {
            
            return this._http.get<any>("http://localhost:3000/api/projects").do(data=>console.log("All"+JSON.stringify(data))).catch(this.handleError);
        }

        getProjectById(id:number):any{
                return this._http.get<any>(`http://localhost:3000/api/project/${id}`).do(data=>console.log("All"+JSON.stringify(data))).catch(this.handleError);

        }
       

    private handleError(err:HttpErrorResponse){

        console.log(err);

        return Observable.throw(err.message);
    }
}