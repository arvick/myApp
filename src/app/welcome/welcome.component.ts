import { Component, OnInit } from '@angular/core';
import { UserListService } from "../user.service";
import { AdminServiceService } from "../admin/admin-service.service";

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {
  eps:object[];
  employeeList: any;
  tempList:any;
  teamList: any;
  errorMessage: any;
  empli:any="test";
  projectList:any;
  constructor(private _userService:UserListService, private _adminService:AdminServiceService) { 


  }

  getEmpList(list):void{

          this._adminService.getEmployeeList(list).subscribe(employeeList=>{this.employeeList=employeeList, this.emp(this.employeeList)},error=>this.errorMessage,()=>{});
          
      

        
    } 
  
  emp(list):any{
      /*this.eps.pop();
      this.eps.push(list);*/
      //this.empli='';
      for(var i=0; i<=list.length-1; i++){
         this.empli +='<li>'+list[i].name+'</li>'
      }
     
      return this.empli;
  }

  ngOnInit() {

    //get all projects
    this._userService.getAllProjects().subscribe(projectList=>{this.projectList=projectList},error=>this.errorMessage);

    // get all teams
    this._adminService.getAllTeams().subscribe(teamList=>{console.log(this.teamList=teamList)},error=>this.errorMessage);

    //this._adminService.getEmployeeList()

  }

}
