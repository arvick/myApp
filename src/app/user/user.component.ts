import { Component, OnInit } from '@angular/core';
import { UserListService } from "../user.service";
import { IUserlist } from "../userlist";

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
  providers:[UserListService]
})
export class UserComponent implements OnInit {

  title = 'app';
  pageTitle:string='Employee List';
  imgWidth:number=30;
  imageShow:boolean=false;
   filterName:string;
  errorMessage:string;
  loader:string;

 
  get listFilter():string{
      return this.filterName;
  }

  set listFilter(value:string){
     this.filterName=value;
     this.filteredList=this.listFilter ? this.performFilter(this.listFilter):this.userLists;
  }

  filteredList:IUserlist[];
  userLists  :IUserlist[]=[];

   constructor(private _userList:UserListService){
    
   // this.listFilter='';

  }

  toggleImage():void{

      this.imageShow=!this.imageShow;

  }


  performFilter(filterText:string):IUserlist[]{
    filterText=filterText.toLocaleLowerCase();
    return this.userLists.filter((userlist:IUserlist)=>userlist.name.toLowerCase().indexOf(filterText)!== -1);
  }

  showRating(message:string):void{
    this.pageTitle="Rating "+ message;

  }

  ngOnInit():void{
      this.loader="wait loading.....";
       this._userList.getUserList().subscribe(userLists=>{this.userLists=userLists
                                                          this.filteredList=this.userLists
                                                        },
                                                                error=>this.errorMessage=<any>error
                                                                   );


                                                                   this.loader=" ";
      
  }

}
