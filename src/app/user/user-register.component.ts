import { Component, OnInit } from '@angular/core';
import { IUserlist } from "../userlist";
import { NgForm } from "@angular/forms/src/forms";
import { UserListService } from "../user.service";
import { FormGroup, FormBuilder } from "@angular/forms";

@Component({
  selector: 'app-user-register',
  templateUrl: './user-register.component.html',
  styleUrls: ['./user-register.component.css'],
  providers:[UserListService]
})
export class UserRegisterComponent implements OnInit {
  _query: string;
  emp: IUserlist[];
  msg:string;
  flag:boolean=false;
   errorMessage:string;
  list:any;
  empForm:FormGroup;
  constructor(private _userService:UserListService, private formbuilder:FormBuilder) {

    this.empForm=this.formbuilder.group({
        id:'', 
        name:'',
        projectId:'',
        designation:'',
        img:'',
        ranking:'',
        empCode:'',
        password:''

    });
   }

  submitForm(form:NgForm):void{
    let formData=new FormData();
     let  fd=new FormData();
        fd.append('id',this.empForm.get('id').value);
        fd.append('project',this.empForm.get('projectId').value);
        fd.append('name',this.empForm.get('name').value);
	       fd.append('designation',this.empForm.get('designation').value);
	       fd.append('ranking',this.empForm.get('ranking').value);
         fd.append('empCode',this.empForm.get('empCode').value);
          fd.append('password',this.empForm.get('password').value);
          fd.append('file',this.empForm.get('img').value);
    //this._query = `${JSON.stringify(formData)}`;

    
    //let test= this._userService.addUser(this._query);

    console.log(JSON.stringify(fd));

     this._userService.addUser(form).subscribe(msg=>{this.msg=msg
                                                        },
                                                                error=>this.errorMessage=<any>error
                                                                   );

  }

     onFileChange(event):void{
     let file = event.target.files[0];
     console.log(file);
    this.empForm.get('img').setValue(file);
  }

  ngOnInit() {

     this._userService.getAllProjects().subscribe(list=>{this.list=list},error=>this.errorMessage=<any>error);

     console.log(this.list);
  }

}
