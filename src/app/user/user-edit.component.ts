import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { IUserlist } from "../userlist";
import { NgForm } from "@angular/forms/src/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { FormGroup, FormBuilder } from "@angular/forms";
import { UserListService } from "../user.service";

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.css'],
  providers:[UserListService]
})
export class UserEditComponent implements OnInit {
  @ViewChild('fileInput') fileInput: ElementRef;
  
  pageTitle:'';
  errorMessage: any;
  _userList: any;
  userLists  :IUserlist[]=[];
  empForm:FormGroup;
  _query: string;
    msg:string;
  fileToUpload:any;
  list:any;
  constructor(private _route:ActivatedRoute, private _router:Router, private _userService:UserListService, private formbuilder:FormBuilder) {
     
    this.empForm=this.formbuilder.group({
        id:'', 
        name:'',
        projectId:'',
        designation:'',
        img:'',
        ranking:'',
        empCode:'',
        password:''

    });

   }

   submitForm(form:NgForm):void{

       let  fd=new FormData();
       
        fd.append('id',this.empForm.get('id').value);
        fd.append('name',this.empForm.get('name').value);
         fd.append('project',this.empForm.get('projectId').value);
	       fd.append('designation',this.empForm.get('designation').value);
	       fd.append('ranking',this.empForm.get('ranking').value);
         fd.append('empCode',this.empForm.get('empCode').value);
          fd.append('password',this.empForm.get('password').value);
          fd.append('file',this.empForm.get('img').value);
          console.log(fd.get('designation'));
     this.msg="test";
    this._userService.editUser(fd).subscribe(msg=>{this.msg=msg
                                                        },
                                                                error=>this.errorMessage=<any>error
                                                                   );
          console.log(this.empForm.get('img').value.filename);
   // this._router.navigate(['/userlist']);                                                               

  }
    onFileChange(event):void{
     let file = event.target.files[0];
     console.log(file);
    this.empForm.get('img').setValue(file);
  }

  ngOnInit() {
    let id= +this._route.snapshot.paramMap.get('id');   

     this._userService.getUser(id).subscribe(userLists=>{this.userLists=userLists,
      this.empForm.setValue({

                id:userLists[0].id,
                name:userLists[0].name,
                projectId:userLists[0].projectId,
                designation:userLists[0].designation,
                img:'',
                ranking:userLists[0].ranking,
                empCode:userLists[0].empCode,
                password:userLists[0].password
                
                

       })
                                                        },
                                                                error=>this.errorMessage=<any>error
                                                                   );


     console.log(this.userLists);        
       
       this._userService.getAllProjects().subscribe(list=>{this.list=list},error=>this.errorMessage=<any>error);
       
  }

}
