import { Component, OnInit } from '@angular/core';
import { UserListService } from "../user.service";
import { ActivatedRoute, Router } from "@angular/router";

@Component({
  selector: 'app-user-delete',
  templateUrl: './user-delete.component.html',
  styleUrls: ['./user-delete.component.css'],
  providers:[UserListService]
})
export class UserDeleteComponent implements OnInit {

  errorMessage:any[];
  msg:string;
  constructor(private _userService:UserListService, private _route:ActivatedRoute, private _router:Router) { }

  
     
    


  ngOnInit() {

    let id= +this._route.snapshot.paramMap.get('id');
      this._userService.removeUser(id).subscribe(msg=>{this.msg=msg
                                                        },
                                                                error=>this.errorMessage=<any>error
                                                                   );

       this._router.navigate(['/userlist']);
       this.msg="Record has been deleted with id "+id+"";
  }

}
