import { Component, OnInit } from '@angular/core';
import { IUserlist } from "./userlist";
import { UserListService } from "./user.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers:[UserListService]
})
export class AppComponent{
 
}
