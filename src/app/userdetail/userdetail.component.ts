import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { IUserlist } from "../userlist";
import { UserListService } from "../user.service";

@Component({
  selector: 'app-userdetail',
  templateUrl: './userdetail.component.html',
  styleUrls: ['./userdetail.component.css'],
  providers:[UserListService]
})
export class UserdetailComponent implements OnInit {
   errorMessage: any;
  userLists  :IUserlist[]=[];
  userId:number;
  projectId:number;
  project:any;
  image:string;
  pageTitle:string;
  userData:IUserlist;
  constructor(private _route:ActivatedRoute, private _router:Router,private _userService:UserListService) {}
 
  goBack():void{
    this._router.navigate(['/userlist']);
  }

  ngOnInit() {
    let id= +this._route.snapshot.paramMap.get('id');
    this.userId=id;

    /*this.userData={"id":1,"name":"paul arockiam", "desg":"UI Developer", "img":"./assets/abdul-informal-pic.jpg","rank":4.5,"empCode":"t1-108147"};
    console.log(id);*/

    this._userService.getUser(id).subscribe(userLists=>{this.userLists=userLists,
                                                    this.pageTitle=userLists[0].name,
                                                    this.projectId=userLists[0].projectId,
                                                    this.image=userLists[0].image,
                                                    this._userService.getProjectById(this.projectId).subscribe(project=>{console.log(this.project=project[0].project_name)},error=>this.errorMessage=<any>error);
                                                        },
                                                                error=>this.errorMessage=<any>error
                                                                   );

            
                                                            
                                                                    
    
  }


}
