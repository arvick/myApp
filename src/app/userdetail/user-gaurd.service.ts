import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot } from "@angular/router";

@Injectable()
export class UserGaurdService implements CanActivate {

  constructor(private _route:Router) { }

  canActivate(route:ActivatedRouteSnapshot):boolean{

    let url=+route.url[1].path;

    if(isNaN(url) || url <=0 )
      {
        alert("url is not matching");
        this._route.navigate(['/userlist']);
        return false;
      }
    return true;
  }

}
