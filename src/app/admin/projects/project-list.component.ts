import { Component, OnInit } from '@angular/core';
import { UserListService } from "../../user.service";

@Component({
  selector: 'app-project-list',
  templateUrl: './project-list.component.html',
  styleUrls: ['./project-list.component.css']
})
export class ProjectListComponent implements OnInit {
  pageTitle:string='Project list';
  projectList:any;
  constructor(private _userService:UserListService) { 


  }

  ngOnInit() {

    this._userService.getAllProjects().subscribe(projectList=>{console.log(this.projectList=projectList)});
  }

}
