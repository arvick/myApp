import { Component, OnInit } from '@angular/core';
import { UserListService } from "../../user.service";
import { FormGroup, FormBuilder } from "@angular/forms";
import { AdminServiceService } from "../admin-service.service";

@Component({
  selector: 'app-add-team',
  templateUrl: './add-team.component.html',
  styleUrls: ['./add-team.component.css']
})
export class AddTeamComponent implements OnInit {

  userLists:any;
  errorMessage:string='';
  teamForm:FormGroup;
  msg:'';
  constructor(private _userService:UserListService, private _formBuilder:FormBuilder, private _adminService:AdminServiceService) {

    this.teamForm=this._formBuilder.group({
      teamName:'',
      teamEmployee:''

    })

   }

   submitTeam():void{

      let teamForm=new FormData();

      teamForm.append('teamName',this.teamForm.get('teamName').value);
      teamForm.append('teamEmployee',this.teamForm.get('teamEmployee').value.join(','));

      console.log(this.teamForm.value);

      this._adminService.addTeam(teamForm).subscribe(msg=>{this.msg=msg},error=>this.errorMessage=<any>error);
   }

  ngOnInit() {

    this._userService.getUserList().subscribe(userLists=>{this.userLists=userLists
                                                        },
                                                                error=>this.errorMessage=<any>error
                                                                   );
  }

}
