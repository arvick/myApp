import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, NgForm } from "@angular/forms";
import { AdminServiceService } from "./admin-service.service";

@Component({
  selector: 'app-addproject',
  templateUrl: './addproject.component.html',
  styleUrls: ['./addproject.component.css'],
  providers:[AdminServiceService]
})
export class AddprojectComponent implements OnInit {
  errorMessage: any;
  prjForm:FormGroup;
  msg:string;

  constructor(private formBuilder:FormBuilder, private _adminService:AdminServiceService) { 

    this.prjForm=this.formBuilder.group({
      projectName:'',
      country:''
    });
  }

  submitProject(form:NgForm):void{

       let prjForm=new FormData();

       prjForm.append('projectName',this.prjForm.get('projectName').value);
       prjForm.append('country',this.prjForm.get('country').value);

       this._adminService.addProject(prjForm).subscribe(msg=>{this.msg=msg},error=>this.errorMessage=<any>error);

  }
  ngOnInit() {
  }

}
