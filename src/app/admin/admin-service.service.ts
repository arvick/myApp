import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Observable } from "rxjs/Observable";

@Injectable()
export class AdminServiceService {

  constructor(private _http:HttpClient) { }

     addProject(query:FormData):any{           
       let _dataUrlPost:string='http://localhost:8082/myApp-server/add-project.php';
            
            return this._http.post<FormData>(_dataUrlPost,query).do(data=>console.log(JSON.stringify(data))).catch(this.handleError);
        }

     addTeam(query:FormData):any{           
       let _dataUrlPost:string='http://localhost:8082/myApp-server/add-team.php';
            
            return this._http.post<FormData>(_dataUrlPost,query).do(data=>console.log(JSON.stringify(data))).catch(this.handleError);
        }


     getAllTeams():Observable<any> {
            
            return this._http.get<any>("http://localhost:8082/myApp-server/list-teams.php").do(data=>console.log("All"+JSON.stringify(data))).catch(this.handleError);
        }

       
    getEmployeeList(list:any):any{

        return this._http.get<any>("http://localhost:8082/myApp-server/get-employees.php?list="+list).do(data=>JSON.stringify(data)).catch(this.handleError);
    }
     
    
   private handleError(err:HttpErrorResponse){

        console.log(err);

        return Observable.throw(err.message);
    }

}
