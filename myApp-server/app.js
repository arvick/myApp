const express = require('express');
const mysql = require('mysql');
const cors = require('cors');
const bodyParser = require('body-parser');

const encodeParser = bodyParser.urlencoded({ extended: false })
const app = express();

app.use(cors());
// app.use(bodyParser);
// creating DB connection 
const db = mysql.createConnection({
    host : 'localhost',
    user : 'angular_user',
    password : 'root',
    database : 'emp_data'
});

//connect

db.connect((err)=>{
    if(err){
        throw err;
    }

    console.log("connection eastablished");
});

const PORT = process.env.PORT || 3000;
app.listen(PORT,()=>{
    console.log(`listining at port ${PORT}`);
} );


app.get('/',(req, res)=>{
    res.send('Welcome to node API Width MySql database');
});

// show all employee list
app.get('/api/employeeList', (req, res)=>{
    const sql = 'select * from user_details';

    db.query(sql, (err, result)=>{
        if(err) throw err;

        res.send(result);
    });
});

//insert into  employee table
app.post('/api/employee', encodeParser, (req, res)=>{
    //  const post ={name:"ramesh", projectId : 3, designation: "web-developer", empCode: "187456", ranking: "4", image: "test.jpg", password: "test"};
    const post = req.body;
    console.log(post);
   // const sql = 'insert into user_details SET ?';

    // db.query(sql, post, (err, result)=>{
    //     if(err) throw err;

    //     res.send('data inserted');
    // });
});

//show employee by id
app.get('/api/employee/:id', (req, res)=>{
    const id = req.params.id;
    const sql = `select * from user_details where id=${id}`;
    db.query(sql,(err, result)=>{
        if(err) throw err;
        
        res.send(result);
    });
});

//insert into  project table
app.post('/api/addProject', (req, res)=>{
    const post ={project_name: "4", country: "test.jpg"};
    const sql = 'insert into projects SET ?';

    db.query(sql, post, (err, result)=>{
        if(err) throw err;

        res.send('data inserted');
    });
});

// show all project list
app.get('/api/projects', (req, res)=>{
    const sql = 'select * from projects';

    db.query(sql, (err, result)=>{
        if(err) throw err;

        res.send(result);
    });
});

// show project by id 
app.get('/api/project/:id', (req, res)=>{
    const id = req.params.id;
    const sql = `select * from projects where prj_id=${id}`;
    db.query(sql,(err, result)=>{
        if(err) throw err;
        
        res.send(result);
    });
});


//insert into  teams table
app.post('/api/addTeam', (req, res)=>{
    const post ={team_name: "4", empoyees: "2,3,4"};
    const sql = 'insert into teams SET ?';

    db.query(sql, post, (err, result)=>{
        if(err) throw err;

        res.send('data inserted');
    });
});

// show all team list
app.get('/api/teams', (req, res)=>{
    const sql = 'select * from teams';

    db.query(sql, (err, result)=>{
        if(err) throw err;

        res.send(result);
    });
});

// show team by id 
app.get('/api/team/:id', (req, res)=>{
    const id = req.params.id;
    const sql = `select * from teams where id=${id}`;
    db.query(sql,(err, result)=>{
        if(err) throw err;
        
        res.send(result);
    });
});